# Coff'Event

## Hi, this project is to show and add new events in a coffee.

### First clone my project :
    ```
    git clone https://gitlab.com/ashiky/coffevent.git
    ```
## This project contain 2 folder

### coffevent_back for the back using Symfony
### coffevent_front for the front whith React

# Go inside of coffevent_back
    ```
    cd coffevent_back
    ```
## Change the ".env.test" file into ".env"
    ###connect to your database, you have an exemple for mysql

## use the command :
    ```
    composer update
    php bin/console doctrine:database:create
    php bin/console doctrine:migrations:migrate
    symfony server:start
    ```

## they are gonna :
### Install all dependances
### Create your dataBase
### Use migration to create table (you should have some error but the data should be created)
### Launch your server
### in that order

## And your symfony server should be good and running

## Go inside of coffevent_front
    ```
    cd coffevent_front
    ```
## use the command :
    ```
    npm install
    npm run dev
    ```

## they are gonna :
    ###Install all dependances
    ###Launch your server
    ###in that order


# And that all you can use the site

# Link to maquette, wireframe and MCD
## https://www.figma.com/file/L9LnopJzHkJS1Aj0W5WsI9/Caff'Event?type=design&node-id=0%3A1&t=cjek9ZYvKmmVP7FL-1