<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\EventsRepository;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use Symfony\Component\Serializer\Annotation\Groups;


#[ORM\Entity(repositoryClass: EventsRepository::class)]
#[ApiResource(
    operations: [
        // Creer
        new Post(normalizationContext: ['groups' => 'events:item']),
        // show one
        new Get(normalizationContext: ['groups' => 'events:item']),
        // show all
        new GetCollection(normalizationContext: ['groups' => 'events:list'])
]
)]
class Events
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['events:list', 'events:item'])]

    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['events:list', 'events:item'])]

    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['events:list', 'events:item'])]

    private ?string $description = null;

    #[ORM\Column(length: 255)]
    #[Groups(['events:list', 'events:item'])]

    private ?string $date = null;

    #[ORM\Column]
    #[Groups(['events:list', 'events:item'])]

    private ?int $number_participant = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['events:list', 'events:item'])]

    private ?string $img = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getNumberParticipant(): ?int
    {
        return $this->number_participant;
    }

    public function setNumberParticipant(int $number_participant): self
    {
        $this->number_participant = $number_participant;

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(string $img): self
    {
        $this->img = $img;

        return $this;
    }
}
