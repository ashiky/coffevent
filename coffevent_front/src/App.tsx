import './App.css'
import Form from './pages/Form'
import Events from './pages/Events'
import FormParticipation from './componant/FormParticipation'
import FormEvent from './componant/FormEvent'
import { Route, Routes } from 'react-router-dom'
import Detail from './pages/Detail'

function App() {

  return (
    <Routes>
      <Route path='/' element={<Events />} />
      <Route path='/form' element={<Form />}>
        <Route path='/form/participation/:idEvent' element={<FormParticipation />} />
        <Route path='/form/create/event' element={<FormEvent />} />
      </Route>
      <Route  path="/event/detail/:idEvent" element={<Detail />} />
    </Routes>
  )
}

export default App
