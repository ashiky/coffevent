import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import NavBar from "../componant/NavBar";
import { Link } from "react-router-dom";

type event = {
    img: string
    title: string
    description: string
    idEvent: GLfloat
    date: string
}

export default function Detail() {
    const [detail, setDetail] = useState<event>()
    let params = useParams();
    useEffect(() => {

        fetch(`http://127.0.0.1:8000/api/events/${params.idEvent}`)
            .then((response) => {
                // console.log(response.json());
                return response.json();
            })
            .then((datas) => {
                console.log(datas);
                setDetail(datas);
                console.log(detail)
            });
    }, [])
    
    return (
        <div className="main">
            <NavBar />
            <div>
                <h1>{detail?.title}</h1>
                <h2>{detail?.date}</h2>
                <p>{detail?.description}</p>
            </div>
            <Link className="link" to={`/form/participation/${params.idEvent}`}>Participez à l'evenement !</Link>
        </div>
    )
}