import NavBar from "../componant/NavBar"
import { Outlet } from 'react-router-dom'

export default function Form(){
    return (
    <div className="form">
        <NavBar />
        <Outlet></Outlet>
    </div>
    )
}