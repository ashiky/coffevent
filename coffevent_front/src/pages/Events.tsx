import Event from "../componant/Event";
import NavBar from '../componant/NavBar'
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
type event = {
        id: 0
        date: ""
        description: ""
        number_participant: 0
        participant: []
        status: false
        title: ""
        img:string
}

export default function Events() {
    const [events, setEvents] = useState <event[]>()
    useEffect(() => {
        fetch("http://127.0.0.1:8000/api/events?page=1")
            .then((response) => response.json())
            .then((datas) => {
                setEvents(datas["hydra:member"]);

            });
    }, [])
    console.log(events)
    return (
        <div className="main">
            <NavBar />
            <Link className="link" to="/form/create/event">Votre evennement !</Link>
            <div className="events">
                {events?.map((event:event,id) => {
                    return (
                        <Event key={id} idEvent={event?.id} img={event?.img} title={event?.title} description={event.description} />
                    )
                })
                }
            </div>
        </div>
    )
}