import { useState } from "react"
import { useParams } from "react-router-dom"

export default function FormParticipation() {

    const [firstname, setFirstname] = useState("")
    const [lastname, setLastname] = useState("")
    const [phone_number, setPhone_number] = useState("")
    let params = useParams();

    async function sendParticipant(event: any) {
        event.preventDefault()
      
            if (firstname && lastname && phone_number != "") {
                fetch('http://127.0.0.1:8000/api/participants', {
                    method: 'post',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        "firstname": firstname,
                        "lastname": lastname,
                        "phone_number": phone_number,
                        "event": `"/api/events/${params.idEvent}"`,
                    })

                }).then(() => {
                    alert('Votre evennement à bien été ajouté !')
                    location.assign("http://localhost:5173/")

                }).catch((error) => {
                    alert(error)
                })
            } else {
                alert("Merci de remplir tout les champs")
            }
       


    }


    return (
        <div>
            <form className="form-participation" action="">
                <p>Prenom</p>
                <input onChange={((val) => setFirstname(val.target.value))} type="text" />
                <p>Nom</p>
                <input onChange={((val) => setLastname(val.target.value))} type="text" />
                <p>Téléphone</p>
                <input onChange={((val) => setPhone_number(val.target.value))} type="text" />
                <button onClick={sendParticipant}>Participer</button>
            </form>
        </div>
    )
}