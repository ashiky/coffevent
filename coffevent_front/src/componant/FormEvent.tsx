import { useState } from "react"


export default function FormEvent() {
    const [title, setTitle] = useState("")
    const [description, setDescription] = useState("")
    const [date, setDate] = useState("")

    async function sendEvent(event:any) {
        event.preventDefault()
        if(title && description && date != ""){
            fetch('http://127.0.0.1:8000/api/events', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "title": title,
                "description": description,
                "date": date,
                "status": false,
                "img": "https://www.baristamagazine.com/wp-content/uploads/2020/03/CalendarPic20-550x367.jpg",
                "numberParticipant": 0
            })

        }).then(() => {
            alert('Votre evennement à bien été ajouté !')
            location.assign("http://localhost:5173/")

        }).catch((error) => {
            alert(error)
        })
        } else {
            alert("Merci de remplir tout les champs")
        }
        
    }
    return (
        <div>
            <form method="post" className="form-participation">
                <p>Titre de l'evenement</p>
                <input onChange={((val) => setTitle(val.target.value))} type="text" />
                <p>Description</p>
                <input onChange={((val) => setDescription(val.target.value))} type="textarea" />
                <p>Date</p>
                <input onChange={((val) => setDate(val.target.value))} type="text" />
                <button onClick={sendEvent} >Envoyer</button>
            </form>
        </div>
    )
}