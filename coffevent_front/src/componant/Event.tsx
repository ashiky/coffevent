import { Link } from "react-router-dom"
type eventProps = {
    img: string
    title: string
    description: string
    idEvent:GLfloat
}

export default function Event(props: eventProps) {
    return (
        <Link to={"/event/detail/" + props.idEvent}>
            <div className="event">
                <img src={props.img} alt="" />
                <h2>{props.title}</h2>
                <p>{props.description}</p>
            </div>
        </Link>
    )
}